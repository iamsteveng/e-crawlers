import { AxiosProxyConfig } from 'axios';
import url from 'url';

export function getProxy(): AxiosProxyConfig | undefined {
  let proxy: AxiosProxyConfig | undefined = undefined;

  const proxyUrl = process.env.PROXY_URL || process.env.FIXIE_URL;
  if (proxyUrl) {
    const parsedUrl = url.parse(proxyUrl);
    const [username, password] = parsedUrl.auth?.split(':') || ['', ''];
    proxy = {
      protocol: parsedUrl.protocol || '',
      host: parsedUrl.hostname || '',
      port: Number.parseInt(parsedUrl.port || '80'),
      auth: {
        username,
        password,
      },
    };
  }

  return proxy;
}
