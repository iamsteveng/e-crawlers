import express from 'express';
import { crawl } from './crawler';

const app = express();
app.use(express.json());

app.get('/r', async (req, res) => {
  const url = req.query.url as string;
  try {
    const result = await crawl(url);
    console.log(result);
    res.json(result).status(200);
  } catch (error) {
    res.status(400).send();
  }
});

const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`Listening to ${port}`);
});

export default app;
