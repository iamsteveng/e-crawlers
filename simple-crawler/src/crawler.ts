import axios, { AxiosRequestConfig } from 'axios';
import encoding from 'encoding';
import * as cheerio from 'cheerio';
import { getProxy } from './proxy';

export async function crawl(url: string) {
  const html = await fetchUrl(url);
  const $ = cheerio.load(html);

  const name = $('span.item_name').text();
  console.log('Found: ' + name);

  const original_price = $('span.double_price').text();
  console.log({ original_price });

  const selling_price = $('span.price2').text().trim();
  console.log({ selling_price });

  const imageElements = $('a.rakutenLimitedId_ImageMain1-3 > img');
  const images = imageElements.map((i, img) => img.attribs['src']).toArray();
  console.log(images);

  const choiceElements = $('span.inventory_choice_name');
  const choices = choiceElements.map((i, ch) => $(ch).text()).toArray();
  console.log(choices);

  const largeImageElements = $('span.sale_desc > img');
  const large_images = largeImageElements
    .map((i, img) => img.attribs['src'])
    .toArray();
  console.log(large_images);

  return {
    name,
    choices,
    original_price,
    selling_price,
    large_images,
    images,
  };
}

async function fetchUrl(url: string) {
  console.log(`Fetching ${url}`);
  // make http call to url
  const proxy = getProxy();
  const config: AxiosRequestConfig = {
    proxy,
    responseType: 'arraybuffer',
  };
  console.log(`Axios config: ${JSON.stringify(config, null, 2)}`);
  const response = await axios.get(url, config).catch((err) => {
    console.log(err);
    throw new Error('Failed to fetch the url');
  });

  if (response.status !== 200) {
    console.log('Received non 200 response code');
    throw new Error('Failed to fetch the url');
  }

  const crawled = encoding.convert(response.data, 'utf-8', 'euc-jp').toString();
  return crawled;
}

// crawl(
//   // 'https://item.rakuten.co.jp/dripcoffee/rich-500-4/'
//   'https://item.rakuten.co.jp/shishang/aldry06-02/'
// );
